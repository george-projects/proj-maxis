
This client library is intended for footer libraries. This will only be loaded during preview or Production view. All items listed here are post HTML load.

Footer library will NOT contain any CSS related items 

This client library should contain general styles and scripts. Everything specific to components should be placed in client libraries below the corresponding components.
